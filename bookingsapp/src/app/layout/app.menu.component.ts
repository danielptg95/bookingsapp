import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { LayoutService } from './service/app.layout.service';

@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html'
})
export class AppMenuComponent implements OnInit {

    model: any[] = [];

    constructor(public layoutService: LayoutService) { }

    ngOnInit() {
        this.model = [
            {
                label: 'Inicio',
                items: [
                    { label: 'Dashboard', icon: 'pi pi-fw pi-home', routerLink: ['/'] }
                ]
            },
            {
                label: 'Administrar',
                items: [
                    { label: 'Busqueda', icon: 'pi pi-fw pi-id-card', routerLink: ['/search'] },
                    { label: 'Mis reservas', icon: 'pi pi-fw pi-id-card', routerLink: ['/mybookings'] }
                ]
            }
        ];
    }
}
