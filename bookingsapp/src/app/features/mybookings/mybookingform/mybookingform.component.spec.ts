import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MybookingformComponent } from './mybookingform.component';

describe('MybookingformComponent', () => {
  let component: MybookingformComponent;
  let fixture: ComponentFixture<MybookingformComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MybookingformComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MybookingformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
