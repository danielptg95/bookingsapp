import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { Booking } from 'src/app/core/models/booking';
import { BookingService } from 'src/app/core/services/booking.service';

@Component({
  selector: 'app-mybookingform',
  templateUrl: './mybookingform.component.html',
  styleUrl: './mybookingform.component.scss'
})
export class MybookingformComponent {
  formGroup: FormGroup;
  isUpdateMode: boolean = false;
  modalLabelButton: 'Crear' | 'Guardar' = 'Crear';
  bookingDate: string;
  mindatebooking = new Date();

  constructor(private fb: FormBuilder, private bookingService: BookingService) { }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      Id: new FormControl({ value: '', disabled: true }, Validators.required),
      Idclient: new FormControl({ value: '', disabled: true }, Validators.required),
      Documentnumber: ['', Validators.required,],
      Name: ['', Validators.required,],
      Amount: [1, Validators.required,],
      Bookingdate: ['', Validators.required,],
    });
 
  }

  public onShowModal(booking: Booking, isUpdateMode: boolean): void {
    this.isUpdateMode = isUpdateMode;
    if (this.isUpdateMode) {
      this.formGroup.patchValue({ Id: booking.idbooking,
                                  Idclient: booking.idclient, 
                                  Documentnumber: booking.documentnumber, 
                                  Name: booking.name, 
                                  Amount: booking.amount, 
                                  Bookingdate: booking.bookingdate });
      this.modalLabelButton = 'Guardar';
    }
  }

  onSubmit() {
    console.log('this.formGroup',this.formGroup);

    if (this.formGroup.get('Id').value == ''){

      console.log('this.formGroup',this.formGroup.get('Bookingdate').value);

      let booking : Booking = { 
        idclient: this.formGroup.get('Idclient').value,
        documentnumber: this.formGroup.get('Documentnumber').value,
        name: this.formGroup.get('Name').value,
        amount: parseInt(this.formGroup.get('Amount').value),
        bookingdate: moment(this.bookingDate).format('DD/MM/YYYY')
      }

      this.bookingService.createBookings(booking).subscribe({
        next: (response) => {
          console.log('response', response);
        },
        error: (e) => console.error('Error', e),
        complete: () => console.info('complete')
      });

    }else {
      console.log('this.formGroup',this.formGroup.get('Bookingdate').value);

      let booking : Booking = { 
        idbooking: this.formGroup.get('Id').value,
        idclient: this.formGroup.get('Idclient').value,
        documentnumber: this.formGroup.get('Documentnumber').value,
        name: this.formGroup.get('Name').value,
        amount: parseInt(this.formGroup.get('Amount').value),
        bookingdate: moment(this.bookingDate).format('DD/MM/YYYY')
      }

      this.bookingService.updateBookings(booking).subscribe({
        next: (response) => {
          console.log('response', response);
        },
        error: (e) => console.error('Error', e),
        complete: () => console.info('complete')
      });
    }
  }
}
