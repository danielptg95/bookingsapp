import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MybookingsComponent } from './mybookings.component';
import { StyleClassModule } from 'primeng/styleclass';
import { CalendarModule } from 'primeng/calendar';
import routes from './mybookings.routes';
import { MybookingformComponent } from './mybookingform/mybookingform.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    declarations: [
        MybookingformComponent,
        MybookingsComponent
    ],
    imports: [
      RouterModule.forChild(routes),
      CommonModule,
      StyleClassModule,
      SharedModule,
      CalendarModule
    ],
    exports: [
      RouterModule
    ]
  })
export class mybookingsModule { }