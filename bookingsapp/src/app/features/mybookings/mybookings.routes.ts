import { Routes } from "@angular/router";
import { MybookingsComponent } from "./mybookings.component";

const routes: Routes = [
    {
        path: '',
        children: [
            { path: '', component: MybookingsComponent }
        ]
    }
];

export default routes;