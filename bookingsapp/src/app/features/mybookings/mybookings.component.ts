import { Component, OnInit, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { Booking } from 'src/app/core/models/booking';
import { Client } from 'src/app/core/models/client';
import { Service } from 'src/app/core/models/service';
import { BookingService } from 'src/app/core/services/booking.service';
import { ClientService } from 'src/app/core/services/client.service';
import { ServiceService } from 'src/app/core/services/service.service';
import { MybookingformComponent } from './mybookingform/mybookingform.component';

@Component({
  selector: 'app-mybookings',
  templateUrl: './mybookings.component.html',
  styleUrl: './mybookings.component.scss'
})
export class MybookingsComponent implements OnInit {
  @ViewChild('form') bookingForm: MybookingformComponent;

  listServices: Service[] = [];
  selectedService: Service;
  listClients: Client[] = [];
  selectedClient: Client;
  rangeDates: string;
  listBookings: Booking[] = [];
  visible: boolean = false;
  header: 'Reservar' | 'Modificar Reserva' = 'Reservar'

  constructor(private serviceService: ServiceService,private clientService: ClientService,
    private bookingService: BookingService
  ) { }

  ngOnInit(): void {
    this.obtenerData();
  }

  obtenerData() {

    this.serviceService.getAllServices().subscribe({
      next: (response) => {
        console.log('response', response);
        this.listServices = response.result;
      },
      error: (e) => console.error('Error', e),
      complete: () => console.info('complete')
    });

    this.clientService.getAllClients().subscribe({
      next: (response) => {
        console.log('response', response);
        this.listClients = response.result;
      },
      error: (e) => console.error('Error', e),
      complete: () => console.info('complete')
    });

  }

  submit(){
    console.log('this.selectedService',this.selectedService);
    console.log('this.selectedClient',this.selectedClient);
    console.log('this.rangeDates',this.rangeDates);

    if(this.rangeDates == null){
      alert('Seleccione un rango de fechas');
      return;
    }
    let date1: string[] = moment(this.rangeDates[0]).format('DD/MM/YYYY').split('/');
    let date2 =moment(this.rangeDates[1]).format('DD/MM/YYYY').split('/');
    var Idate = new Date(parseInt(date1[2]), parseInt(date1[1]), parseInt(date1[0]));
    var Fdate = new Date(parseInt(date2[2]), parseInt(date2[1]), parseInt(date2[0]));

    let getBookings={
      Documentnumber: null,
      idservice: this.selectedService == null ? null  : this.selectedService.idservice,
      Idclient: this.selectedClient == null ? null  : this.selectedClient.idclient,
      Idate: Idate,
      Fdate: Fdate
    }

    this.bookingService.getbydocumentBookings(getBookings).subscribe({
      next: (response) => {
        console.log('response', response);
        this.listBookings = response.result;
      },
      error: (e) => console.error('Error', e),
      complete: () => console.info('complete')
    });

  }

  onCreateOrUpdate(value?: Booking, create?: boolean) {
    this.visible = true;
    this.header = value.idclient != null ? 'Modificar Reserva' : 'Reservar';
    this.bookingForm.onShowModal(value, create);
  }

}
