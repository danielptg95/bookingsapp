import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SearchComponent } from './search.component';
import { StyleClassModule } from 'primeng/styleclass';
import { CalendarModule } from 'primeng/calendar';
import routes from './search.routes';
import { BookingFormComponent } from './booking-form/booking-form.component';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    declarations: [
        BookingFormComponent,
        SearchComponent
    ],
    imports: [
      RouterModule.forChild(routes),
      CommonModule,
      StyleClassModule,
      SharedModule,
      CalendarModule
    ],
    exports: [
      RouterModule
    ]
  })
export class SearchModule { }