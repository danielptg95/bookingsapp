import { Routes } from "@angular/router";
import { SearchComponent } from "./search.component";

const routes: Routes = [
    {
        path: '',
        children: [
            { path: '', component: SearchComponent }
        ]
    }
];

export default routes;