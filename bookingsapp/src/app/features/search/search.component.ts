import { Component, OnInit, ViewChild } from '@angular/core';
import { Client } from 'src/app/core/models/client';
import { ClientService } from 'src/app/core/services/client.service';
import { BookingFormComponent } from './booking-form/booking-form.component';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrl: './search.component.scss'
})
export class SearchComponent implements OnInit {
  @ViewChild('form') bookingForm: BookingFormComponent;
  
  clientes: Client[] = [];
  visible: boolean = false;
  header: 'Reservar' | 'Modificar Reserva' = 'Reservar'

  constructor(private clientService: ClientService) { }

  ngOnInit(): void {
    this.obtenerData();
  }

  obtenerData() {

    this.clientService.getAllClients().subscribe({
      next: (response) => {
        console.log('response', response);
        this.clientes = response.result;
      },
      error: (e) => console.error('Error', e),
      complete: () => console.info('complete')
    });

  }

  onCreateOrUpdate(value?: Client, create?: boolean) {
    this.visible = true;
    this.header = value.idclient != null ? 'Modificar Reserva' : 'Reservar';
    this.bookingForm.onShowModal(value, create);
  }
}
