import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Booking } from 'src/app/core/models/booking';
import { Client } from 'src/app/core/models/client';
import { BookingService } from 'src/app/core/services/booking.service';
import * as moment from "moment";

@Component({
  selector: 'app-booking-form',
  templateUrl: './booking-form.component.html',
  styleUrl: './booking-form.component.scss'
})
export class BookingFormComponent {
  formGroup: FormGroup;
  isUpdateMode: boolean = false;
  modalLabelButton: 'Crear' | 'Guardar' = 'Crear';
  bookingDate: string;
  mindatebooking = new Date();

  constructor(private fb: FormBuilder, private bookingService: BookingService) { }

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      Id: new FormControl({ value: '', disabled: true }, Validators.required),
      Idclient: new FormControl({ value: '', disabled: true }, Validators.required),
      ClientName: new FormControl({ value: '', disabled: true }, Validators.required),
      Documentnumber: ['', Validators.required,],
      Name: ['', Validators.required,],
      Amount: [1, Validators.required,],
      Bookingdate: ['', Validators.required,],
    });
 
  }

  public onShowModal(client: Client, isUpdateMode: boolean): void {
    this.isUpdateMode = isUpdateMode;
    if (this.isUpdateMode) {
      // this.formGroup.patchValue({ Idclient: client.idclient, 
      //                             Documentnumber: ''client.'', 
      //                             Name: client.icon, 
      //                             Amount: client.app, 
      //                             Bookingdate });
      this.modalLabelButton = 'Guardar';
    } else {
      this.formGroup.patchValue({ Idclient: client.idclient, ClientName: client.clientname });
      this.modalLabelButton = 'Crear';
    }
  }

  onSubmit() {
    console.log('this.formGroup',this.formGroup);

    if (this.formGroup.get('Id').value == ''){
      // this.store.dispatch(menuActions.create({
      //   menu: {
      //     name: this.formGroup.get('name').value,
      //     icon: this.formGroup.get('icon').value,
      //     app: this.idAppSelected
      //   }
      // }));

      console.log('this.formGroup',this.formGroup.get('Bookingdate').value);

      let booking : Booking = { 
        idclient: this.formGroup.get('Idclient').value,
        documentnumber: this.formGroup.get('Documentnumber').value,
        name: this.formGroup.get('Name').value,
        amount: parseInt(this.formGroup.get('Amount').value),
        bookingdate: moment(this.bookingDate).format('DD/MM/YYYY')
      }

      this.bookingService.createBookings(booking).subscribe({
        next: (response) => {
          console.log('response', response);
        },
        error: (e) => console.error('Error', e),
        complete: () => console.info('complete')
      });

    }else {
      // this.store.dispatch((menuActions.update({
      //   menu: {
      //     id: this.formGroup.get('id').value,
      //     name: this.formGroup.get('name').value,
      //     icon: this.formGroup.get('icon').value,
      //     app: this.idAppSelected
      //   }
      // })));
    }
  }

}
