export interface Response {
    success: boolean;
    result: any;
}