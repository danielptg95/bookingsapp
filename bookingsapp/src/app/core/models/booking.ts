export interface Booking {
    idbooking?: string
    idclient?: string,
    documentnumber: string,
    name: string,
    amount: number,
    bookingdate: string
}