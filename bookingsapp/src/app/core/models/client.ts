export interface Client {
    idclient?: string,
    clientnit: string,
    clientname: string,
    idservice: string,
    description: string,
    disabled: boolean,
    creationdate: string
}