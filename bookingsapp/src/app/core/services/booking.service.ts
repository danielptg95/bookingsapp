import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Response } from 'src/app/shared/models/response';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  constructor(private http: HttpClient) { }

  createBookings(data : any){
    let url=`${environment.baseURL}/Api/Booking/create`;
    return this.http.post<Response>(url,data);
  }

  getAllBookings(){
    let url=`${environment.baseURL}/Api/Booking/getall`;
    return this.http.get<Response>(url);
  }

  getbydocumentBookings(data : any){
    let url=`${environment.baseURL}/Api/Booking/getbydocument`;
    return this.http.post<Response>(url,data);
  }

  updateBookings(data : any){
    let url=`${environment.baseURL}/Api/Booking/update`;
    return this.http.put<Response>(url,data);
  }

}
